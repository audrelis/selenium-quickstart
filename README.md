Selenium Quickstart
=======================

Prerequisites:
- JDK 8
- Maven

1. Open a terminal window/command prompt
2. Clone this project by running command `git clone https://gitlab.com/audrelis/selenium-quickstart.git`
3. To run Selenium tests use `mvn clean test`
4. To generate test report use `mvn allure:report`
5. To view test report type `mvn site jetty:run` and open page at http://localhost:8080

To run tests in IntelliJ IDEA follow these steps:
1. Open Run/Debug configurations window by selecting Run -> Edit Configurations...
2. Expand Defaults and select TestNG entry
3. Under JDK settings pass webdriver binary path to VM options e.g. -Dwebdriver.chrome.driver=src\test\resources\selenium_standalone_binaries\windows\googlechrome\64bit\chromedriver.exe

### Options for tests

You can specify which browser to use by using one of the following switches:

- -Dbrowser=firefox
- -Dbrowser=chrome
- -Dbrowser=ie
- -Dbrowser=edge
- -Dbrowser=opera
- -Dbrowser=safari
- -Dbrowser=phantomjs

You don't need to worry about downloading the IEDriverServer, MicrosoftWebDriver, chromedriver, operadriver, or geckodriver binaries, this project will do that for you automatically.

You can specify a grid to connect to where you can choose your browser, browser version and platform:

- -Dremote=true 
- -DseleniumGridURL=http://{username}:{accessKey}@ondemand.saucelabs.com:80/wd/hub 
- -Dplatform=xp 
- -Dbrowser=firefox 
- -DbrowserVersion=54

You can even specify multiple threads (you can do it on a grid as well!):

- -Dthreads=2

You can also specify a proxy to use

- -DproxyEnabled=true
- -DproxyHost=localhost
- -DproxyPort=8080
