package lt.tieto.selenium.demo.page.gmail;

import lt.tieto.selenium.demo.page.AbstractPage;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class GmailInboxPage extends AbstractPage<GmailInboxPage> {

    @FindBy(id = ":2i")
    private WebElement primaryTab;

    public GmailInboxPage() {
        this.get();
    }

    @Override
    protected void isLoaded() throws Error {
        try {
            if (!primaryTab.isDisplayed()) {
                throw new Error("Primary tab is not visible yet");
            }
        } catch (NoSuchElementException nsee) {
            throw new Error(nsee);
        }
    }

}
