package  lt.tieto.selenium.demo.page;

import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.Wait;

import java.util.concurrent.TimeUnit;

public abstract class AbstractPage<T extends AbstractPage<T>> extends AbstractLoadableComponent<T> {

    public static final int DEFAULT_TIMEOUT_IN_SECONDS = 20;
    public static final int DEFAULT_POLLING_INTERVAL = 500;

    private final String url;

    public AbstractPage() {
        super(DEFAULT_TIMEOUT_IN_SECONDS);
        this.url = null;
    }

    public AbstractPage(String url) {
        this(url, DEFAULT_TIMEOUT_IN_SECONDS);
    }

    public AbstractPage(String url, int timeoutInSeconds) {
        super(timeoutInSeconds);
        this.url = url;
    }

    public final String getUrl() {
        return url;
    }

    public String getTitle() {
       return getDriver().getTitle();
    }

    @Override
    protected void load() {
        super.load();
        if (url != null) {
            getDriver().get(url);
        }
    }

    protected void waitUntilElementVisible(WebElement element) {
        Wait<WebDriver> wait = new FluentWait<>(getDriver()).withTimeout(
                DEFAULT_TIMEOUT_IN_SECONDS, TimeUnit.SECONDS).pollingEvery(DEFAULT_POLLING_INTERVAL,
                TimeUnit.MILLISECONDS).ignoring(NoSuchElementException.class);
        wait.until(ExpectedConditions.visibilityOf (element));
    }

}
