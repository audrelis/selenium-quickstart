package lt.tieto.selenium.demo.page;

import lt.tieto.selenium.demo.tests.SeleniumTestBase;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.SlowLoadableComponent;
import org.openqa.selenium.support.ui.SystemClock;

public abstract class AbstractLoadableComponent<T extends AbstractLoadableComponent<T>>
        extends SlowLoadableComponent<T> {

    private final WebDriver driver;
    private final int timeoutInSeconds;

    public AbstractLoadableComponent(final int timeoutInSeconds) {
        super(new SystemClock(), timeoutInSeconds);
        this.driver = SeleniumTestBase.getDriver();
        this.timeoutInSeconds = timeoutInSeconds;
        this.load();
    }

    public final WebDriver getDriver() {
        return driver;
    }

    public final int getTimeoutInSeconds() {
        return timeoutInSeconds;
    }

    @Override
    protected void load() {
        PageFactory.initElements(getDriver(), this);
    }

}
