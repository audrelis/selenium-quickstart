package lt.tieto.selenium.demo.page.gmail;

import lt.tieto.selenium.demo.page.AbstractPage;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class GmailLoginPage extends AbstractPage<GmailLoginPage> {

    @FindBy(id = "identifierNext")
    private WebElement nextButton;

    @FindBy(id = "passwordNext")
    private WebElement signInButton;

    @FindBy(id = "identifierId")
    private WebElement emailInput;

    @FindBy(name = "password")
    private WebElement passwordInput;

    @FindBy(css = "div#password div[jsname='B34EJ']")
    private WebElement passwordValidationMessage;

    private GmailLoginPage(String url) {
        super(url);
    }

    public static GmailLoginPage open(String url) {
        GmailLoginPage page = new GmailLoginPage(url);
        page.get();
        return page;
    }

    public GmailLoginPage pressNext() {
        nextButton.click();
        waitUntilElementVisible(passwordInput);
        return this;
    }

    public void signIn() {
        signInButton.click();
    }

    public GmailLoginPage enterEmail(String email) {
        emailInput.sendKeys(email);
        return this;
    }

    public GmailLoginPage enterPassword(String password) {
        passwordInput.sendKeys(password);
        return this;
    }

    public String getPasswordValidationMessage() {
        waitUntilElementVisible(passwordValidationMessage);
        return passwordValidationMessage.getText();
    }

    @Override
    protected void isLoaded() throws Error {
        try {
            if (!nextButton.isDisplayed()) {
                throw new Error("Next button is not visible yet");
            }
        } catch (NoSuchElementException nsee) {
            throw new Error(nsee);
        }
    }

}
