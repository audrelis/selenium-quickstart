package lt.tieto.selenium.demo.data;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;

@Configuration
@PropertySource("classpath:/data/${active.env:dev}/test-data.properties")
public class TestData {

    @Autowired
    private Environment environment;

    public String getDataEntry(String key) {
        return environment.getProperty(key);
    }

}