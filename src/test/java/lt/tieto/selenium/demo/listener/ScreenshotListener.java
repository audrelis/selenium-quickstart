package lt.tieto.selenium.demo.listener;

import lt.tieto.selenium.demo.tests.SeleniumTestBase;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.remote.Augmenter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.ITestResult;
import org.testng.TestListenerAdapter;
import io.qameta.allure.Attachment;

import java.text.SimpleDateFormat;
import java.util.Date;

public class ScreenshotListener extends TestListenerAdapter {

    private static final Logger LOG = LoggerFactory.getLogger(ScreenshotListener.class);

    public static final String SCREENSHOT_FILE_DATE_TIME_FORMAT_MASK = "yyyy-MM-dd HH:mm:ss.SSS";

    @Override
    public void onTestFailure(ITestResult failingTest) {
        try {
            takeScreenshot(new SimpleDateFormat(SCREENSHOT_FILE_DATE_TIME_FORMAT_MASK).format(new Date()));
        } catch (Exception e) {
            LOG.error("Unable to capture screenshot", e);
        }
    }

    @Attachment(value = "screenshot {dateTime}", type = "image/png")
    public byte[] takeScreenshot(String dateTime) throws Exception {
        WebDriver driver = SeleniumTestBase.getDriver();
        try {
            return ((TakesScreenshot) driver).getScreenshotAs(OutputType.BYTES);
        } catch (ClassCastException cce) {
            driver = new Augmenter().augment(driver);
            return ((TakesScreenshot) driver).getScreenshotAs(OutputType.BYTES);
        }
    }

}
