package lt.tieto.selenium.demo.tests;

import lt.tieto.selenium.demo.page.gmail.GmailInboxPage;
import lt.tieto.selenium.demo.page.gmail.GmailLoginPage;
import org.testng.annotations.Test;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.is;
import static org.hamcrest.core.StringContains.containsString;

public class GmailLoginPageTest extends SeleniumTestBase {

    @Test
    public void loginUsingValidCredentials() {
        performLogin(getDataEntry("login"), getDataEntry("password"));
        GmailInboxPage inboxPage = new GmailInboxPage();
        assertThat(inboxPage.getTitle(), containsString("Inbox"));
    }

    @Test
    public void loginUsingInvalidCredentials() {
        GmailLoginPage loginPage = performLogin(getDataEntry("login"), "WrongPassword");
        assertThat(loginPage.getTitle(), is("Gmail"));
        assertThat(loginPage.getPasswordValidationMessage(), is("Wrong password. Try again."));
    }

    private GmailLoginPage performLogin(String email, String password) {
        GmailLoginPage loginPage = GmailLoginPage.open(getDataEntry("base.url"));
        loginPage.enterEmail(email)
                .pressNext()
                .enterPassword(password)
                .signIn();
        return loginPage;
    }

}
