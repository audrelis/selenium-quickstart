package lt.tieto.selenium.demo.tests;

import lt.tieto.selenium.demo.config.DriverFactory;
import lt.tieto.selenium.demo.data.TestData;
import lt.tieto.selenium.demo.listener.ScreenshotListener;
import org.openqa.selenium.WebDriver;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.Listeners;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

@Listeners(ScreenshotListener.class)
public class SeleniumTestBase {

    private static List<DriverFactory> webDriverThreadPool = Collections.synchronizedList(new ArrayList<DriverFactory>());
    private static ThreadLocal<DriverFactory> driverThread;
    private TestData testData;

    @BeforeSuite(alwaysRun = true)
    public void initData() {
        ApplicationContext context; context = new AnnotationConfigApplicationContext(TestData.class);
        testData = context.getBean(TestData.class);
    }

    public String getDataEntry(String key) {
        return testData.getDataEntry(key);
    }

    @BeforeSuite(alwaysRun = true)
    public static void instantiateDriverObject() {
        driverThread = ThreadLocal.withInitial(() -> {
            DriverFactory driverFactory = new DriverFactory();
            webDriverThreadPool.add(driverFactory);
            return driverFactory;
        });
    }

    public static WebDriver getDriver() {
        return driverThread.get().getDriver();
    }

    @AfterMethod(alwaysRun = true)
    public static void clearCookies() throws Exception {
        getDriver().manage().deleteAllCookies();
    }

    @AfterSuite(alwaysRun = true)
    public static void closeDriverObjects() {
        webDriverThreadPool.forEach(DriverFactory::quitDriver);
    }

}